#include <stdio.h>

#define IN  1
#define OUT 0
#define MAX 31

main()
{
    int c, state, length, i, j;
    int charcounts[MAX];
    
    state = OUT;
    length = 0;

    for (i = 0; i < MAX; i++)
        charcounts[i] = 0;  

    while((c = getchar()) != EOF) {
        if(c == ' ' || c == '\n' || c == '\t') {
            if(state != OUT) {
                state = OUT;
                if (length > 0)
                {
                    if (length < MAX) charcounts[length]++;
                    else charcounts[MAX]++;
                    length = 0;
            }
            }
        } else if (state == OUT) { 
                state = IN;
                length = 1;   
        } else {
            length++;
        }
    }

    for(i = 1; i < MAX; i++) {
        printf("%3d |", i);
        for(j = 0; j < charcounts[i]; j++) {
            putchar('#');
        }
        putchar('\n');
    }
}
