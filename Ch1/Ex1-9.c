#include <stdio.h>

main()
{
    int c, blank;
    blank = 0;
    printf("Input characters and blanks will be shortened\n");
    while ((c = getchar()) != EOF)
        if (c == ' ') {
            if(!blank) {
                blank = 1;
                putchar(c);
            }
        } else {
            blank = 0;
            putchar(c);
        }
}
