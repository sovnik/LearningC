#include <stdio.h>

main()
{
    int c, nblanks, ntabs, nlines;
    nblanks = 0;
    ntabs = 0;
    nlines = 0;
    while ((c = getchar()) != EOF)
        if (c == ' ')
            nblanks++;
        if (c == '\t')
            ntabs++;
        if (c == '\n')
            nlines++;
    printf("Blanks: %d, Tabs: %d, Lines: %d\n", nblanks, ntabs, nlines);
}
