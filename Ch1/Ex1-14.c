#include <stdio.h>

#define CHARS 128

main()
{
    int c, i, j;
    int numchars[CHARS + 1];

    for(i = 0; i < CHARS; i++) {
        numchars[i] = 0;
    }

    while((c = getchar()) != EOF){
       numchars[c]++; 
    }

    for(i = 0; i < CHARS; i++) {
        if(numchars[i] > 0) {
            printf("%3d |", i);
            for(j = 0; j < numchars[i]; j++) {
                putchar('#');
            }
            putchar('\n');
        }
    }
}
