#include <stdio.h>

float ftoc (int degrees);

main()
{
    int fahr;
    float celcius;
    printf("Fahr\tCelcius\n");

    for (fahr = 0; fahr <= 300; fahr += 20) {
        celcius = ftoc(fahr);
        printf("%3d\t%6.1f\n", fahr, celcius);
    }
}

float ftoc (int degrees)
{
    return (5.0 / 9.0) * (degrees - 32);
}
