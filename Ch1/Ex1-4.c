#include <stdio.h>

main()
{
    float fahr, celcius;
    float lower, upper, step;

    lower = -17;
    upper = 148;
    step = 11;
    printf("Celcius\tFahr\n");
    celcius = lower;
    while (celcius <= upper) {
        fahr = ((celcius * 9) / 5) + 32;
        printf("%3.0f\t%6.1f\n", celcius, fahr);
        celcius = celcius + step;
    }
}
